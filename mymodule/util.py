from . import core

def do_count(iterations=9):
    """calls some_func equal to iterations
    """
    for i in range(iterations):
        core.hello(i)
